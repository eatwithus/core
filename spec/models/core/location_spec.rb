require 'spec_helper'

describe Location do
  before(:all) do
  	Location.destroy_all
  end

  describe "Associations" do
  	it { should have_many(:outlets) }
  end

  describe "Validations" do
  	it { should validate_presence_of :country }
  	it { should validate_presence_of :state }
  	it { should validate_presence_of :city }
  	it { should validate_presence_of :area }

  	%w(country state city area).each do |field|
      context 'when #{field} is empty' do
	      it "raise error" do          
	        lambda do
		        Location.create! FactoryGirl.attributes_for(:location, field.to_sym => "")
		      end.should raise_error(Mongoid::Errors::Validations)
	      end
	    end
   	end
  end

  describe "identifier method" do
    context "when call the identifier?" do
      it "it will be true and return location id" do
        @location = Location.create! FactoryGirl.attributes_for(:location)
        @location.identifier.should == @location.id.to_s
      end
    end
  end

end
