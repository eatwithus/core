require 'spec_helper'

module Core
  describe TaxMode do
    it{should belong_to(:store)}
    it{should have_and_belongs_to_many(:outlets)}
  end
end
