require 'spec_helper'

module Core
  describe OrderItem do
    it{should belong_to(:order)}
    it{should belong_to(:menu_item)}
  end
end
