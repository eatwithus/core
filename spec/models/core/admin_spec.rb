require 'spec_helper'

describe Admin do

  before(:all) do
    Admin.destroy_all
    @admin = FactoryGirl.create(:admin)
  end

  describe "Validation" do
    it {should ensure_inclusion_of(:role).in_array(%w[super_admin developer support sales]) }
    ["", "kes34dlf", "r.g.@gobu", "gobu@1989", "123@"].each do |email|
      context 'with invalid email' do
        it "raise error" do          
          lambda do
            FactoryGirl.create(:admin, :email => email)
          end.should raise_error(Mongoid::Errors::Validations)
        end
      end
    end

    ["", "wrongpassword"].each do |password|
      context 'with incorrect / mismatch password' do
        it "raise error" do          
          lambda do
            FactoryGirl.create(:admin, password: password, password_confirmation: password)
          end.should raise_error(Mongoid::Errors::Validations)
        end
      end
    end  
  end

  describe "identifier method" do
    context "when call the identifier?" do
      it "it will be true and return location id" do
        @admin.identifier.should == @admin.id.to_s
      end
    end
  end

  describe "identifier method" do
    context "when call the identifier?" do
      it "it will be true and return location id" do
        @admin.full_name.should == @admin.first_name+" "+@admin.last_name
      end
    end
  end

  context "when call the set_auth_token" do
    it "it will generate a token and update user" do
      @admin.auth_token = nil
      @admin.save
      @admin.set_auth_token
      @admin.auth_token.should_not == nil
    end
  end

  context "when call the remove_auth_token" do
    it "it will generate a token and update user" do
      @admin.remove_auth_token
      @admin.auth_token.should == nil
    end
  end

end
