require 'spec_helper'

describe SocialPage do

  before(:all) do
    Store.destroy_all
    @store = FactoryGirl.create(:store)
    Outlet.destroy_all    
  end

  describe "Associations" do
    it { should be_embedded_in(:outlet) }
  end
  
  describe "Validations" do
  	%w(contact_no cuisines payment_options).each do |field|
	  context 'when #{field} is empty' do
	  	it "raise error" do
	      #@store.outlets.create! FactoryGirl.attributes_for(:outlet)
	      #FactoryGirl.create(:store, :outlet, :social_pages)
		end	      
	  end
	end
  end
end
