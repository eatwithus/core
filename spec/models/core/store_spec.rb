require 'spec_helper'

describe Store do
  before(:all) do
    Store.destroy_all
    @store = FactoryGirl.create(:store)
  end

  describe "associations" do  	
    it { should embed_one :invoice }
    it { should have_many :outlets }
    it { should have_many :menu_cards }
  end
  
  describe "Validation" do
  	it { should validate_presence_of :name }
    it { should validate_presence_of :contact_no }

    context 'when contact_no is wrong' do
	  it "raise error" do
	    lambda do
	      Store.create! FactoryGirl.attributes_for(:store, :contact_no => ["23423"])
	    end.should raise_error(Mongoid::Errors::Validations)
	  end
	end
  end
end
