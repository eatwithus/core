require 'spec_helper'

describe Outlet do
  before(:all) do
    Store.destroy_all
    @store = FactoryGirl.create(:store)
    Outlet.destroy_all
  end

  describe "Constants" do
	it { Outlet.should have_constant(:STORE_TYPES) }
	it { Outlet.should have_constant(:CUISINES) }
	it { Outlet.should have_constant(:PAYMENT_OPTIONS) }
	it { Outlet.should have_constant(:RESERVATION_TYPES) }
  end

  describe "Associations" do
  	it { should embed_many(:social_pages) }
  	it { should belong_to(:store) }
  	it { should belong_to(:location) }
  	it { should belong_to(:menu_card) }
  end

  describe "Validations" do
  	it { should validate_presence_of :contact_no }
    it { should validate_presence_of :cuisines }
    it { should validate_presence_of :payment_options }
    it { should validate_presence_of :store_type }
    it { should validate_presence_of :reservation }
    it { should validate_presence_of :facilities }
    it { should validate_presence_of :features }

	context 'when store_type is wrong(not in list)' do
	  it "raise error" do
	    lambda do
	      @store.outlets.create! FactoryGirl.attributes_for(:outlet, :store_type => "wrong store_type")
	    end.should raise_error(Mongoid::Errors::Validations)
	  end
	end

   	context 'when reservation is wrong(not in list)' do
	  it "raise error" do
	    lambda do
		  @store.outlets.create! FactoryGirl.attributes_for(:outlet, :reservation => "wrong reservation")
		end.should raise_error(Mongoid::Errors::Validations)
	  end
	end

	context 'when store_type is valid' do
	  it "response should be success" do
	    expect do
	      @store.outlets.create! FactoryGirl.attributes_for(:outlet, :store_type => "Restaurant")
        end.to change(Outlet, :count).by(1)
	  end
	end

   	context 'when reservation is empty' do
	  it "response should be success" do
		expect do
		  @store.outlets.create! FactoryGirl.attributes_for(:outlet, :reservation => "Not Required")
        end.to change(Outlet, :count).by(1)
	  end
	end

	context 'when contact_no is wrong' do
	  it "raise error" do
	    lambda do
	      Store.create! FactoryGirl.attributes_for(:store, :contact_no => ["23423"])
	    end.should raise_error(Mongoid::Errors::Validations)
	  end
	end
  end
end
