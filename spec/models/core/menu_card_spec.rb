require 'spec_helper'

describe MenuCard do
  before(:all) do
  	Store.destroy_all
    MenuCard.destroy_all
    @store = FactoryGirl.create(:store)
  end

  describe "Associations" do
  	it { should have_many(:outlets) }
    it { should have_many(:menu_items) }
  	it { should belong_to(:store) }
  end

  describe "Validations" do
  	it { should validate_presence_of :name } 	

  	%w(name).each do |field|
      context 'when #{field} is empty' do
	    it "raise error" do          
	      lambda do
		    @store.menu_cards.create! FactoryGirl.attributes_for(:menu_card, field.to_sym => "")
		  end.should raise_error(Mongoid::Errors::Validations)
	    end
	  end
   	end
  end
end
