require 'spec_helper'

describe MenuItem do
    before(:all) do
  	Store.destroy_all
  	Outlet.destroy_all
    MenuCard.destroy_all
    @store = FactoryGirl.create(:store)
    @menu_card = @store.menu_cards.create! FactoryGirl.attributes_for(:menu_card)
    @outlet = @store.outlets.create! FactoryGirl.attributes_for(:outlet, menu_card_id: @menu_card.id)
  end

  describe "Associations" do
  	it { should belong_to(:menu_card) }
  end

  describe "Validations" do
  	it { should validate_presence_of :title }

  	%w(title).each do |field|
      context 'when #{field} is empty' do
	    it "raise error" do          
	      lambda do
		    @menu_card.menu_items.create! FactoryGirl.attributes_for(:menu_item, field.to_sym => "")
		  end.should raise_error(Mongoid::Errors::Validations)
	    end
	  end
   	end
  end
end
