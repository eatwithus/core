# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :store, class: 'Store' do
	name "Managewithus"
	logo "managewithus.png"
	contact_no ["9003659336"]
  end
end
