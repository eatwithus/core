# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :v1_customer, :class => 'V1::Customer' do
    full_name "MyString"
    email "MyString"
    contact_no "MyString"
    dob "MyString"
    visit_count "MyString"
    order_count "MyString"
  end
end
