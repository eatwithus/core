# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :facebook, class: SocialPage do
    provider "facebook"
    url "https://www.facebook.com/eatwithusindia"
    uid "eatwithusindia"
  end
  factory :twitter, class: SocialPage do
    provider "twitter"
    url "https://twitter.com/eatwithusindia"
    uid "eatwithusindia"
  end
end