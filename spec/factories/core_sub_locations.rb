# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :core_sub_location, :class => 'SubLocation' do
    city "MyString"
    area "MyString"
    sub "MyString"
    latitude 1.5
    longitude 1.5
  end
end
