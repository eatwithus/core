# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :core_delivery_log, :class => 'DeliveryLog' do
    latitude 1.5
    longitude 1.5
    references ""
  end
end
