# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do  
  factory :sun_operational_1, class: RestaurantTime do
    day "sun"
    open "10.30"
    close "13:30"
    timing_type "Operational"
  end
  factory :sun_operational_2, class: RestaurantTime do
    day "sun"
    open "15.30"
    close "23:30"
    timing_type "Operational"
  end
  factory :sun_delivery_1, class: RestaurantTime do
    day "sun"
    open "10.30"
    close "13:30"
    timing_type "Delivery"
  end
  factory :sun_delivery_2, class: RestaurantTime do
    day "sun"
    open "15.30"
    close "21:30"
    timing_type "Delivery"
  end
end
