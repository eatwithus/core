# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :location, :class => 'Location' do
    country "India"
    state "Karnataka"
    city "Bangalore"
    area "Koramangala"
  end
end
