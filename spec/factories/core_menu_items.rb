# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :menu_item, class: 'MenuItem' do
    title "MyString"
    description "MyString"
    price Money.new(1000, 'INR')
  end
end
