# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :invoice do
    name "MyString"
    address "MyString"
    contact_no "MyString"
    note "MyString"
  end
end
