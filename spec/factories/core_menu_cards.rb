# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :menu_card, class: 'MenuCard' do
    name "MyCard"
    description "MyString"
  end
end
