# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :outlet, class: 'Outlet' do
	store_type "Restaurant"
	description "Test description"
	cuisines ["Afghani", "African"]
	address "Kaleena Agrahara"
	contact_no ["9003659336"]
	latitude "12.80561845206578"
	longitude "77.58191536931145"
	cost_for_two Money.new(1000, 'INR')
	minimum_delivery_cost Money.new(300, 'INR')
	payment_options ["Cash", "Debit/Credit Card"]
	happy_hours "Monday to Friday 4pm to 7pm"
	buffet_time "Monday to Friday 7pm to 10pm"
	reservation "Recommended"
	keywords ["mengna","bangalore"]
	published true
	published_on Time.now
	verification_status ""
	notification_number "9003659336"
	notification_email "rakesh71187@gmail.com"
	delivery_cost Money.new(1000, 'INR')
	free_delivery_distance 1
	cost_delivery_distance 1
	facilities ["Dine In", "Delivery", "Catering", "Take Away"]
    features ["Sheesha", "Bar", "Air Condition"]
  end
end