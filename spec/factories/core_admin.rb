# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :admin do
    first_name 'Demo'
    last_name 'User'
    city 'bangalore'
    email 'testing_user2@gmail.com'
    password 'password'
    password_confirmation 'password'
    role 'developer'
  end
end
