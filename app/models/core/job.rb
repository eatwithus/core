module Core
  class Job
    include Mongoid::Document
    include Mongoid::Slug
    include Mongoid::Timestamps

    store_in collection: "jobs"

    paginates_per 10
    field :title
    field :location, type: Array
    field :description
    field :visibility, type: Boolean
    field :page_title
    field :meta_description
    field :meta_keywords, type: Array
    field :created_by_id
    slug :title

    scope :visibles, -> {where(visibility: true)}

    embeds_many :job_applications, class_name: "Core::JobApplication"

    # Automatic Method call after save a record
    after_save :cleanup_cache

    # Validation
    validates_presence_of :title
    validates_presence_of :location
    validates_presence_of :description

    # Method
    # Cleanup cache data
    def cleanup_cache
      Rails.cache.delete("visible_jobs")
      Rails.cache.delete("job_count")
      Rails.cache.delete(["job", id])
      Rails.cache.delete(["job", slug])
    end

    # get and show cache data
    def self.cached_visibles
      Rails.cache.fetch("visible_jobs") {self.visibles}
    end
    
    # find a record from cache
    def self.cached_find(id)
      Rails.cache.fetch(["job", id]) {find(id)}
    end

    # get cache count
    def self.cached_count
      Rails.cache.fetch(["job_count"]) {Core::Job.all.size}
    end
  end
end
