module Core
  class Store
    include Mongoid::Document
    include Mongoid::Timestamps
    include Mongoid::Paranoia
    include Mongoid::Versioning

    store_in collection: "stores"

    ## Addtional Fields
    field :name
    field :contact_no, type: Array
    field :subdomain
    mount_uploader :logo, Core::LogoUploader

    ## Association
    belongs_to :location, class_name: 'Core::Location'

    embeds_one :invoice, class_name: 'Core::Invoice'
    embeds_many :bank_accounts, class_name: 'Core::BankAccount'

    has_many :outlets, class_name: 'Core::Outlet'
    has_many :menu_cards, class_name: 'Core::MenuCard'
    has_many :tax_modes, class_name: 'Core::TaxMode'
    has_and_belongs_to_many :users, class_name: 'Core::User'
    has_many :pictures, as: :picturable, class_name: 'Core::Picture'

  end
end
