module Core
  class MenuCard
    include Mongoid::Document
    include Mongoid::Timestamps
    include Mongoid::Paranoia
    include Mongoid::Versioning
    max_versions 3

    store_in collection: "menu_cards"

    field :name
    field :description

    ## Associations
    has_many :menu_items, class_name: 'Core::MenuItem', dependent: :destroy
    has_many :outlets, class_name: 'Core::Outlet'
    belongs_to :store, class_name: 'Core::Store'
    accepts_nested_attributes_for :menu_items

    ## Validation
    validates_presence_of :name

  end
end
