module Core
  class Location
    include Mongoid::Document
    include Mongoid::Timestamps

    store_in collection: "locations"

    field :country
    field :state
    field :city
    field :area

    ## Associations
    has_many :stores, class_name: 'Core::Store'
    has_many :outlets, class_name: 'Core::Outlet'

    ## Validation
    validates_presence_of :country, :state, :city, :area
    validates_uniqueness_of :city, scope: [:state, :country, :area]
    validates_uniqueness_of :area, scope: [:country, :state, :city]

  end
end
