module Core
  class User
    include Mongoid::Document
    # Include default devise modules. Others available are:
    # , :lockable, :timeoutable and :omniauthable
    devise :database_authenticatable, :registerable, :lockable, :confirmable,
      :recoverable, :rememberable, :trackable, :validatable

    # Constatnts
    ROLES = ['owner', 'manager', 'user']

    ## Database authenticatable
    field :email,              type: String, default: ""
    field :encrypted_password, type: String, default: ""

    ## Recoverable
    field :reset_password_token,   type: String
    field :reset_password_sent_at, type: Time

    ## Rememberable
    field :remember_created_at, type: Time

    ## Trackable
    field :sign_in_count,      type: Integer, default: 0
    field :current_sign_in_at, type: Time
    field :last_sign_in_at,    type: Time
    field :current_sign_in_ip, type: String
    field :last_sign_in_ip,    type: String

    ## Confirmable
    field :confirmation_token,   type: String
    field :confirmed_at,         type: Time
    field :confirmation_sent_at, type: Time
    field :unconfirmed_email,    type: String # Only if using reconfirmable

    ## Lockable
    field :failed_attempts, type: Integer, default: 0 # Only if lock strategy is :failed_attempts
    field :unlock_token,    type: String # Only if unlock strategy is :email or :both
    field :locked_at,       type: Time

    field :available_outlets, type: Array, default: []
    field :first_name
    field :last_name
    field :gender, default: 'male'
    field :role
    field :auth_token
    mount_uploader :avatar, Core::AvatarUploader

    ## Associations
    has_and_belongs_to_many :stores, class_name: 'Core::Store'

    ## Validations
    # validates :email, uniqueness: {case_sensitive: false, conditions: -> { unscoped.where(deleted_at: nil) } }
    # Validation
    validates :role, inclusion: {in: ROLES.map(&:to_s) }
    validates :gender, inclusion: {in: ['male', 'female']}

    ## Public Methods
    def full_name
      [first_name, last_name].join(" ")
    end

    ROLES.each do |role|
      define_method("#{role}?") do
        self.role == role.to_s
      end
    end

    def set_auth_token
      self.update_attribute(:auth_token, SecureRandom.hex(8)) unless auth_token
    end

    def remove_auth_token
      self.update_attribute(:auth_token, nil)
    end
  end
end
