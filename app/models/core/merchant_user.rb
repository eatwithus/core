module Core
  class MerchantUser
    include Mongoid::Document

    store_in collection: "merchant_users"

    paginates_per 20

    # Include default devise modules. Others available are:
    # :confirmable, :lockable, :timeoutable and :omniauthable
    devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :lockable

    ## Database authenticatable
    field :email,              type: String, default: ""
    field :encrypted_password, type: String, default: ""

    ## Recoverable
    field :reset_password_token,   type: String
    field :reset_password_sent_at, type: Time

    ## Rememberable
    field :remember_created_at, type: Time

    ## Trackable
    field :sign_in_count,      type: Integer, default: 0
    field :current_sign_in_at, type: Time
    field :last_sign_in_at,    type: Time
    field :current_sign_in_ip, type: String
    field :last_sign_in_ip,    type: String

    ## Confirmable
    # field :confirmation_token,   type: String
    # field :confirmed_at,         type: Time
    # field :confirmation_sent_at, type: Time
    # field :unconfirmed_email,    type: String # Only if using reconfirmable

    ## Lockable
    field :failed_attempts, type: Integer, default: 0 # Only if lock strategy is :failed_attempts
    field :unlock_token,    type: String # Only if unlock strategy is :email or :both
    field :locked_at,       type: Time

    field :first_name
    field :last_name
    field :contact_no
    field :role, default: "user"
    field :gender, default: "male"
    
    # autoload :true

    ROLES  = [:admin, :moderator, :user]

    validates :role, inclusion: {in: ROLES.map(&:to_s)}, presence: true
    validates :gender, inclusion: {in: ["male", "female"]}, presence: true
    validates :email, uniqueness: {case_sensitive: false, conditions: -> { unscoped.where(deleted_at: nil) } }
    validates :contact_no, format: { with: /\d{6,12}/, message: "invalid" }

    belongs_to :merchant, class_name: "Core::Merchant"

    def full_name
      [first_name, last_name].join(" ")
    end
  end
end
