module Core
  class Notification
    include Mongoid::Document
    include Mongoid::Timestamps

    store_in collection: 'notifications'

    field :notification_number
    field :notification_email
    field :email_verificaiton, type: Boolean
    field :notify_orders_via_sms, default: true
    field :notify_orders_via_email, default: true

    embedded_in :outlet
  end
end
