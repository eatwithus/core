module Core
  class Order
    include Mongoid::Document
    include Mongoid::Timestamps
    include Mongoid::Paranoia
    include Mongoid::Versioning
    max_versions 3

    store_in collection: "orders"

    field :order_type
    field :order_status, default: "draft"
    field :payment_type
    field :total, type: Money
    field :description, default: ""
    field :name
    field :address
    field :landmark
    field :city
    field :state
    field :pincode, type: Integer
    field :contact_no
    field :invoice_no
    field :oid
    field :accepted_by_id
    field :accepted_on, type: DateTime
    field :call_for_pickup_time, type: DateTime
    field :pickedup_time, type: DateTime
    field :picked_by
    field :delivered_time, type: DateTime
    field :delivered_to
    field :canceled_time
    field :completed_time, type: DateTime
    field :total_amount
    field :settled, type: Boolean
    field :settlement_description
    field :delivery_charges
    field :paid_back, type: Boolean
    field :canceled_by_id
    field :cancel_title
    field :cancel_description

    ORDER_TYPE= ['dine_in', 'web_order', 'delivery_order']
    ORDER_STATUS= ['draft', 'accepted', 'canceled', 'ready_for_delivery', 'picked_up_for_delivery', 'delivered', 'completed', 'other', 'call_for_pickup', 'canceled_by_restaurant', 'canceled_by_customer']

    ## Associations
    embeds_many :order_logs, class_name: 'Core::OrderLog'
    has_many :order_items, class_name: 'Core::OrderItem'
    belongs_to :outlet, class_name: 'Core::Outlet'
    belongs_to :merchant, class_name: 'Core::Merchant'
    belongs_to :customer, class_name: 'Core:Customer'
    belongs_to :coupon, class_name: 'Core::Coupon'
    has_and_belongs_to_many :delivery_executives, class_name: 'Core::DeliveryExecutive'
    belongs_to :sub_location, class_name: 'Core::SubLocation'

    # after_create :trigger_pusher
    # after_update :trigger_pusher

    accepts_nested_attributes_for :order_items, allow_destroy: true

    def call_for_pickup(time)
      _time = (time || Time.now)
      self.update_attributes({order_status: "call_for_pickup", call_for_pickup_time: _time})
      self.order_logs.create({status: "call_for_pickup", message: "Order called for Pickup at #{_time.strftime("%d, %b %Y - %T")}"})
      self
    end

    def accept!(time, accepted_by)
      _time = (time || Time.now)
      self.update_attributes({order_status: "accepted", accepted_on: _time, accepted_by_id: accepted_by.id.to_s})
      self.order_logs.create({status: "accepted", message: "Order was accepted by #{accepted_by.full_name} at #{_time.strftime("%d, %b %Y - %T")}"})
      self
    end

    def picked_up!(time, picked_by, object=nil)
      _time = (time || Time.now)
      self.update_attributes({order_status: "picked_up", pickedup_time: _time, picked_by: picked_by})
      self.order_logs.create({status: "picked_up", message: "Order Picked up #{picked_by} on #{_time.strftime("%d, %b %Y - %T")}"})
      if object && object.is_a?(DeliveryExecutive)
        object.orders << self
        object.save
      end
      self
    end

    def delivered!(time, customer_name)
      _time = (time || Time.now)
      self.update_attributes({order_status: "delivered", delivered_time: _time, delivered_to: customer_name})
      self.order_logs.create({status: "delivered", message: "Order delivered to #{customer_name || N/A} on #{_time.strftime("%d, %b %Y - %T")}"})
      self
    end

    def canceled!(time, canceled_by, cancel_title, cancel_description)
      _time = (time || Time.now)
      self.update_attributes({order_status: "canceled", canceled_time: _time, canceled_by_id: canceled_by.id.to_s, cancel_title: cancel_title, cancel_description: cancel_description})
      self.order_logs.create({status: "canceled", message: "Order canceled on #{_time.strftime("%d, %b %Y - %T")}"})
      self
    end

    def canceled_by_customer!(time)
      _time = (time || Time.now)
      self.update_attributes({order_status: "canceled_by_customer", canceled_time: _time})
      self.order_logs.create({status: "canceled_by_customer", message: "Order cancled by customer on #{_time.strftime("%d, %b %Y - %T")}"})
      self
    end

    def canceled_by_restaurant!(time)
      _time = (time || Time.now)
      self.update_attributes({order_status: "canceled_by_restaurant", canceled_time: _time})
      self.order_logs.create({status: "canceled_by_restaurant", message: "Order cancled by restaurant on #{_time.strftime("%d, %b %Y - %T")}"})
      self
    end

    def trigger_pusher
      Pusher.app_id =  "77094"
      Pusher.key = "7b2e7c2ee2dcc3df94ad"
      Pusher.secret = "f0ca709f828572629bb4"
      data = Order.all.to_a
      Pusher.trigger('my_notifications', 'notification', data)
    end

    def redeem_coupon(code)
      coupon = self.outlet.coupons.find_by(coupon_code: code) rescue nil
      unless coupon.nil? && !coupon.valid_coupon? && !(coupon.constraint[:minimum_order].present? ? coupon.constraint[:minimum_order] <= self.total : true)
        coupon.orders << self
        coupon.redeem_count += 1
        coupon.save
        @changed_amount = 0
        if coupon.coupon_type == "percentage off"
          @changed_amount = ((self.total.cents * coupon.coupon_value)/100)
          self.total.cents += changed_amount 
        elsif coupon.coupon_type == "amount off"
          @changed_amount = coupon.coupon_value
          self.total.cents += @changed_amount
        else
        end
        self.update_compensation(coupon,@changed_amount)
        self.save
      else
        false
      end
    end

    def update_compensation(coupon,amount)
      coupon.couponable_type
    end

    def payment_mode
      if payment_type == "online_payment"
        "Online Payment"
      elsif payment_type == "cod"
        "Cash on Delivery(COD)"
      end
    end

    def status
      if order_status == "draft"
        "Order Initiated"
      elsif order_status == "call_for_pickup"
        "Call for Pickup"
      elsif order_status == "Picked up"
        "Way to delivery"
      elsif order_status == "picked_up_for_delivery"
        "Way to delivery"
      elsif order_status == "delivered"
        "Delivered to Customer"
      elsif order_status == "delivered"
        "Delivered to Customer"
      elsif order_status == "completed"
        "Completed / Closed"
      end
    end
  end
end
