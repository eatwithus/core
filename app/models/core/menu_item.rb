module Core
  class MenuItem
    include Mongoid::Document
    include Mongoid::Timestamps
    include Mongoid::Paranoia
    include Mongoid::Versioning
    max_versions 3

    store_in collection: "menu_items"

    field :title
    field :description
    field :price, type: Money
    field :pure_veg, type: Boolean
    field :category

    CATEGORIES = Core::Food::FOOD_CATEGORIES

    ## Associations
    belongs_to :food, class_name: 'Core::Food'
    belongs_to :menu_card, class_name: 'Core::MenuCard'
    has_many :pictures, as: :picturable, dependent: :destroy, class_name: 'Core::Picture'
    has_many :order_items, class_name: 'Core::OrderItem'

    # Validation
    validates_presence_of :price, :title
  end
end
