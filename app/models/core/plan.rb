module Core
  class Plan
    include Mongoid::Document
    include Mongoid::Timestamps
    include Mongoid::Paranoia
    include Mongoid::Versioning
    max_versions 3

    store_in collection: "plans"

    field :title
    field :count, type: Integer
    field :price, type: Money
    field :duration, type: Integer
    field :features
    field :free_plan, type: Boolean, default: false

    ## Associations
  end
end
