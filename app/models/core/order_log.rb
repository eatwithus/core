module Core
  class OrderLog
    include Mongoid::Document
    include Mongoid::Timestamps
    store_in collection: "order_logs"

    field :status
    field :message

    embedded_in :order, class_name: 'Core::Order'
  end
end
