module Core
  class Merchant
    include Mongoid::Document

    store_in collection: "merchants"

    field :brand_name
    field :company_name
    field :is_active, type: Boolean, default: false
    field :url
    field :address
    field :contact_no
    field :auth_key, default: ->{ SecureRandom.hex(8) }
    field :auth_secret, default: ->{ SecureRandom.hex(8) }
    field :order_update_url

    validates :brand_name, presence: true
    validates :company_name, presence: true
    validates :url, presence: true, format: URI::regexp(%w(http https))
    validates :auth_key, presence: true, uniqueness: true
    validates :auth_secret, presence: true
    validates :merchant_users, associated: true
    validates :contact_no, format: { with: /\d{6,12}/, message: "invalid" }

    ## Associations
    has_many :merchant_users, class_name: "Core::MerchantUser"
    has_many :coupons, as: :couponable, class_name: "Core::Coupon"
    has_many :orders, class_name: "Core::Order"

    ## Public Methods
    def reset_auth_secret!
      self.update_attribute(:auth_secret,  SecureRandom.hex(8))
    end

    def active!
      self.update_attribute(:is_active, true)
    end

    def inactive!
      self.update_attribute(:is_active, false)
    end
 end
end
