module Core
  class OrderItem
    include Mongoid::Document

    store_in collection: "order_items"

    field :price, type: Money
    field :quantity, type: Integer
    field :misc
    field :name

    ## Associations
    belongs_to :order, class_name: 'Core::Order'
    belongs_to :menu_item, class_name: 'Core::MenuItem'

    def item_price
      Money.new(self.price.fractional,"inr").to_f
    end

  end
end
