module Core
  class Admin
    include Mongoid::Document
    include Mongoid::Timestamps
    include Mongoid::Paranoia
    include Mongoid::Versioning
    max_versions 3

    store_in collection: "admins"

    # Include default devise modules. Others available are:
    # :confirmable, :lockable, :timeoutable and :omniauthable
    devise :database_authenticatable, :registerable,
           :recoverable, :rememberable, :trackable, :validatable

    ## Database authenticatable
    field :email,              type: String, default: ""
    field :encrypted_password, type: String, default: ""

    ## Recoverable
    field :reset_password_token,   type: String
    field :reset_password_sent_at, type: Time

    ## Rememberable
    field :remember_created_at, type: Time

    ## Trackable
    field :sign_in_count,      type: Integer, default: 0
    field :current_sign_in_at, type: Time
    field :last_sign_in_at,    type: Time
    field :current_sign_in_ip, type: String
    field :last_sign_in_ip,    type: String

    ## Confirmable
    # field :confirmation_token,   type: String
    # field :confirmed_at,         type: Time
    # field :confirmation_sent_at, type: Time
    # field :unconfirmed_email,    type: String # Only if using reconfirmable

    ## Lockable
    # field :failed_attempts, type: Integer, default: 0 # Only if lock strategy is :failed_attempts
    # field :unlock_token,    type: String # Only if unlock strategy is :email or :both
    # field :locked_at,       type: Time

    ## Additional Fields
    field :first_name, type: String
    field :last_name,  type: String
    field :role,       type: String
    field :auth_token, type: String
    field :city,       type: String, default: ''

    ## Constants
    ROLES = %w(super_admin developer support sales)


    ## Associations
    has_many :coupons, as: :couponable, class_name: 'Core::Coupon'


    ## Validations
    validates :role, inclusion: {in: ROLES}
    validates :email, uniqueness: {case_sensitive: false, conditions: -> { unscoped.where(deleted_at: nil) } }

    ## Public Methods
    def full_name
      [first_name, last_name].join(" ")
    end

    def set_auth_token
      self.update_attribute(:auth_token, SecureRandom.hex(8)) unless auth_token
    end

    def remove_auth_token
      self.update_attribute(:auth_token, nil)
    end
  end
end
