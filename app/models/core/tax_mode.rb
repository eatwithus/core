module Core
  class TaxMode
    include Mongoid::Document
    include Mongoid::Timestamps
    include Mongoid::Paranoia
    include Mongoid::Versioning
    max_versions 3

    store_in collection: "tax_modes"

    field :name
    field :value, type: Float
    field :value_type, default: "percentage"
    field :active, type: Boolean, default: true
    field :default, type: Boolean, default: false
    field :created_by

    ## Associations
    belongs_to :store, class_name: 'Core::Store'
    has_and_belongs_to_many :outlets, class_name: 'Core::Outlet'

    validates_presence_of :name, :value, :value_type
  end
end
