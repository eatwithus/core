module Core
  class Invoice
    include Mongoid::Document

    store_in collection: "invoices"

    field :name
    field :address
    field :contact_no
    field :note

    ## Associations
    embedded_in :store, class_name: 'Core::Store'
  end
end
