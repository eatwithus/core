module Core
  class DeliveryLog
    include Mongoid::Document
    include Mongoid::Timestamps

    field :latitude, type: Float
    field :longitude, type: Float
    field :log_time, type: DateTime
    field :accuracy, type: Float
    field :altitude, type: Float
    field :altitude_accuracy, type: Float
    field :speed, type: Float
    field :heading, type: Float
    belongs_to :delivery_executive

    validates_presence_of :latitude
    validates_presence_of :longitude
  end
end
