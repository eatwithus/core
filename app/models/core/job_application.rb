module Core
  class JobApplication
    include Mongoid::Document

    field :first_name
    field :last_name
    field :contact_number
    field :cover_letter
    field :resume
    field :email

    embedded_in :job, class_name: "Core::Job"

    # Validation
    validates_uniqueness_of :email, scope: [:job_id]

    # Automatic method call after a record create
    after_create :send_mail

    # Method
    # get full_name
    def full_name
      [first_name, last_name].join(" ")
    end

    private
    # mail sending
    def send_mail
      StaticMailer.job_application_reply(self).deliver
      StaticMailer.job_application_notifier(self).deliver
    end
  end
end
