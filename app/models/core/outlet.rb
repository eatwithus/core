require "validator/list_validator"
require 'mongoid'

module Core
  class Outlet

    include Mongoid::Document
    include Mongoid::Slug
    include Mongoid::Timestamps
    include Mongoid::Paranoia
    include Mongoid::Versioning
    max_versions 3

    searchkick locations: ["lat_lng"], autocomplete: ['name', 'area']

    store_in collection: "outlets"

    field :store_type
    field :description
    field :cuisines, type: Array
    field :address
    field :contact_no, type: Array
    field :latitude, type: Float
    field :longitude, type: Float
    field :cost_for_two, type: Money
    field :payment_options, type: Array
    field :happy_hours
    field :buffet_time
    field :reservation
    field :keywords, type: Array
    field :published, type: Boolean
    field :features, type: Array
    field :facilities, type: Array
    field :published_on, type: DateTime
    field :categories
    slug :slug_attr
    field :closed_deal, type: Integer
    field :pure_veg
    field :area
    field :open_hours

    before_save :update_area

    def update_area
      unless area.present?
        self.area = location.area
      end
    end

    def search_data
      {
        id: self.id.to_s,
        store_type: store_type,
        cuisines: cuisines,
        cost_for_two: cost_for_two,
        payment_options: payment_options,
        happy_hours: happy_hours | "",
        buffet_time: buffet_time | "",
        reservation: reservation,
        address: address,
        keywords: keywords,
        features: features,
        facilities: facilities,
        name: store.name,
        lat_lng: [self.latitude, self.longitude]
      }
    end

    ## Constants
    STORE_TYPES = ["Restaurant", "Star", "Bakery", "Catering", "Resort", "Pub"]
    CUISINES = ["Afghani", "African", "American", "Andhra", "Arabian", "Argentinian", "Asian", "Australian", "Bakery", "Balti", "Bangladeshi", "Belgian", "Bengali", "Biryani", "Brazilian", "British", "Burmese", "Cafe", "Cambodian", "Cantonese", "Caribbean", "Chettinad", "Chinese", "Coffee and Tea", "Contemporary", "Continental", "Cuban", "Desserts", "Egyptian", "Ethiopian", "European", "Fast Food", "Filipino", "French", "Fujian", "German", "Goan", "Greek", "Gujarati", "Healthy Food", "Hyderabadi", "Indian", "Indonesian", "International", "Iranian", "Irish", "Italian", "Japanese", "Juices", "Kashmiri", "Kerala", "Konkan", "Korean", "Latin American", "Lebanese", "Lounge", "Lucknowi", "Maharashtrian", "Malaysian", "Malwani", "Mangalorean", "Mediterranean", "Mexican", "Middle Eastern", "Mongolian", "Moroccan", "Mughlai", "Multi Cuisine", "Nepalese", "North Indian", "Others", "Pakistani", "Peranakan", "Persian", "Peruvian", "Pizza", "Portuguese", "Pub", "Rajasthani", "Raw Meats", "Russian", "Sea Food", "Sichuan", "Singaporean", "South Indian", "Spanish", "Sri Lankan", "Steakhouse", "Street Food", "Tea", "Tex-Mex", "Thai", "Tibetan", "Turkish", "Vietnamese"]
    PAYMENT_OPTIONS = ['Debit/Credit Card', 'Cash', 'Meal Coupon']
    RESERVATION_TYPES = ['Not Required', 'Recommended', 'Must Required']
    FACILITIES = ["Dine In", "Delivery", "Catering", "Take Away"]
    FEATURES = ["Sheesha", "Bar", "Air Condition", "Wifi", "Live Music", "Pure veg", "Halal", "Smoking Area", "Outdoor Seating", "Play Area", "Drive In", "Buffet", "Happy Hours"]


    ## Associations
    belongs_to :store, class_name: 'Core::Store'
    belongs_to :menu_card, class_name: 'Core::MenuCard'
    belongs_to :location, class_name: 'Core::Location'
    has_many :pictures, as: :picturable, class_name: 'Core::Picture'
    has_many :coupons, as: :couponable, class_name: 'Core::Coupon'
    has_many :orders, class_name: 'Core::Order'
    has_and_belongs_to_many :tax_modes, class_name: 'Core::TaxMode'
    has_and_belongs_to_many :customers, class_name: 'Core::Customer'
    embeds_many :social_pages, class_name: 'Core::SocialPage'
    embeds_one :delivery, class_name: 'Core::Delivery'
    embeds_one :notification, class_name: 'Core::Notification'
    embeds_many :operational_times, class_name: "Core::OutletTime" do
      def initialize
        @target.select { |timing| timing.timing_type == "Operational"}
      end
    end
    embeds_many :delivery_times, class_name: "Core::OutletTime" do
      def initialize
        @target.select { |timing| timing.timing_type == "Delivery"}
      end
    end

    accepts_nested_attributes_for :notification, allow_destroy: true
    accepts_nested_attributes_for :delivery, allow_destroy: true
    accepts_nested_attributes_for :operational_times, allow_destroy: true
    accepts_nested_attributes_for :delivery_times, allow_destroy: true

    ## Validations
    validates :cuisines, presence: true, list: {constant: CUISINES}
    validates :facilities, presence: true, list: {constant: FACILITIES}
    validates :location, presence: true
    validates :features, list: {constant: FEATURES}
    validates :payment_options, presence: true, list: {constant: PAYMENT_OPTIONS}
    validates_inclusion_of :store_type, in: STORE_TYPES
    validates_inclusion_of :reservation, in: RESERVATION_TYPES
    validates_presence_of :contact_no
    validate :validate_contact_no

    ## Public Methods
    def name
      self.store.name rescue nil
    end

    def area
      self.location.area
    end

    def menu_items
      self.menu_card.menu_items rescue []
    end

    def pure_veg
      self.facilities.include?("Pure veg")
    end

    def should_index?
      self.published
    end

    def opened?
      opened = false
      today_times = self.operational_times.map {|o| o if o.day.downcase == Time.now.strftime("%A").downcase }.compact rescue nil
      begin
        today_times.each do |time|
          start_time = nil
          end_time = nil
          time_start = time.open.split(" ")
          if time_start[1].downcase == "pm"
            start_time = Time.parse(time_start[0]) + 12.hours
          else
            start_time = Time.parse(time_start[0])
          end
          time_end = time.close.split(" ")
          if time_end[1].downcase == "pm"
            end_time = Time.parse(time_end[0]) + 12.hours
          else
            end_time = Time.parse(time_end[0])
          end

          if (start_time..end_time).cover?(Time.now)
            return true
          end
        end
      rescue
        return false
      end
      false
    end

    ## Protected Methods

    ## Private MethodS
    private
    def validate_contact_no
      self.contact_no.each do |cn|
        errors.add(:contact_no, "not valid") if(cn.length < 7 || cn.length > 13)
      end unless self.contact_no.nil?
    end

    def slug_attr
      [self.store.name, self.location.area].join(' ')
    end
  end
end
