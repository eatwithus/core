module Core
  class OutletTime
    include Mongoid::Document
    include Mongoid::Timestamps
    include Mongoid::Paranoia

    store_in collection: "outlet_times"

    field :day, type: String
    field :open, type: String
    field :close, type: String
    field :timing_type, type: String

    index({ timing_type: 1 })

    ## Embeded Relation
    embedded_in :outlet, :inverse_of => :operational_times
    embedded_in :outlet, :inverse_of => :delivery_times
  end
end
