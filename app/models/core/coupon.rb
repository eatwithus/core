module Core
  class Coupon
    include Mongoid::Document
    include Mongoid::Timestamps
    include Mongoid::Paranoia
    include Mongoid::Versioning
    max_versions 3

    store_in collection: "coupons"

    field :coupon_code, default: ->{ SecureRandom.hex(5) }
    field :coupon_type
    field :start_date, type: Date
    field :expiry_date, type: Date
    field :max_count, type: Integer
    field :redeem_count, type: Integer
    field :coupon_value, type: Float
    field :description
    field :constraint, type: Hash

    ## Constants
    COUPON_TYPES = ["percentage off","amount off", "add-on"]

    ## Associations
    belongs_to :couponable, polymorphic: true, class_name: 'Core::Coupon'
    has_and_belongs_to_many :outlets, class_name: 'Core::Outlet'
    has_many :orders, class_name: 'Core::Order'

    ## Validations
    validates_inclusion_of :coupon_type, in: COUPON_TYPES

    ## Public Methods
    def valid_coupon?
      (Date.today.between?(self.start_date..self.expiry_date) && (self.redeem_count < self.max_count))
    end
  end
end
