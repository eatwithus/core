module Core
  class SocialPage
    include Mongoid::Document
    include Mongoid::Timestamps
    include Mongoid::Paranoia

    store_in collection: "social_pages"

    field :provider
    field :url
    field :uid

    ## Associations
    embedded_in :outlet, class_name: 'Core::Outlet'

    ## Validations
    validates_presence_of :provider, :url, :uid

  end
end
