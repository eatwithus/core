module Core
  class BankAccount
    include Mongoid::Document
    include Mongoid::Timestamps

    store_in collection: "bank_accounts"

    field :bank_name
    field :account_name
    field :account_number
    field :ifsc_code
    field :branch_name
    field :branch_city
    field :branch_area
    field :address
    field :contact_no
    field :default, type: Boolean

    ## Associations
    embedded_in :store, class_name: 'Core::Store', :inverse_of => :bank_accounts
    validates_presence_of :bank_name, :account_name, :account_number, :ifsc_code, :branch_name, :address, :contact_no
  end
end
