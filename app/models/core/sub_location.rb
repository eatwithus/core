module Core
  class SubLocation
    include Mongoid::Document
    field :city, type: String
    field :area, type: String
    field :sub, type: String
    field :latitude, type: Float
    field :longitude, type: Float
  end
end
