module Core
  class Customer
    include Mongoid::Document
    include Mongoid::Timestamps

    store_in collection: "customers"

    field :name
    field :email
    field :contact_no
    field :address
    field :land_mark
    field :pincode
    field :city
    field :state
    field :visited_count, type: Integer
    field :order_count, type: Integer
    field :date_of_birth, type: DateTime

    ## Associations
    has_many :orders, class_name: 'Core::Order'
    has_and_belongs_to_many :outlets, class_name: 'Core::Outlet'

    ## Validations
    validates :email, presence: true, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, on: :create }, uniqueness: {case_sensitive: false, conditions: -> { unscoped.where(deleted_at: nil) } }
    # validates :contact_no, numericality: { only_integer: true }, length: { in: 6..12 }
    validates :contact_no, format: { with: /\d{6,12}/, message: "invalid" }
  end
end
