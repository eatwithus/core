module Core
  class Picture
    include Mongoid::Document
    include Mongoid::Timestamps
    include Mongoid::Paranoia
    include Mongoid::Versioning
    max_versions 3

    store_in collection: "pictures"

    mount_uploader :image, Core::PictureUploader
    field :cover_picture

    ## Validation
    validates :image, :presence => true

    ## associations
    belongs_to :picturable, polymorphic: true, class_name: 'Core::Picture'
  end
end
