module Core
  class Food
    include Mongoid::Document
    include Mongoid::Timestamps

    store_in collection: "foods"

    field :title
    field :food_category
    field :pure_veg, type: Boolean
    field :is_child, type: Boolean
    field :parent_id, type: Integer
    field :keywords
    field :description
    field :created_by_id, type: Integer
    field :approved_by_id, type: Integer
    field :approved_at, type: DateTime
    field :published, type: Boolean

    ## Constatnts
    FOOD_CATEGORIES = ["Cake", "Soup", "Starter", "Main Course", "Bread", "Rice", "Gravy", "Soft Drink", "Dessert", "Pastry"]

    ## Associations
    belongs_to :parent, class_name: 'Core::Food'
    belongs_to :created_by, class_name: 'Core::Admin'
    belongs_to :approved_by, class_name: "Core::Admin"
    has_many :pictures, as: :picturable, class_name: 'Core::Picture'
    has_many :menu_items, class_name: 'Core::MenuItem'

    ## Validation
    validates :title, presence: true, length: {maximum: 30}
    validates_inclusion_of :food_category, in: FOOD_CATEGORIES.map(&:to_s)
  end
end
