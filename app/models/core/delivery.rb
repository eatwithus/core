module Core
  class Delivery
    include Mongoid::Document

    store_in collection: "delivery"

    field :free_delivery_distance
    field :delivery_cost_per_km, type: Integer
    field :minimum_delivery_cost, type: Integer

    ## Associations
    embedded_in :outlet, class_name: 'Core::Outlet'
  end
end
