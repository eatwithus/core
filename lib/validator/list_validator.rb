class ListValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    constant_values = options[:constant].map(&:downcase)
    if value && value.find {|a| !constant_values.include?(a.downcase)}
      record.errors.add attribute, 'is not included in the list' 
    end
  end
end
