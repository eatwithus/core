Core::Engine.routes.draw do
  devise_for :delivery_executives, class_name: "Core::DeliveryExecutive"
  devise_for :merchant_users, class_name: "Core::MerchantUser"
  devise_for :users, class_name: "Core::User"
  devise_for :admins, class_name: "Core::Admin"
end
