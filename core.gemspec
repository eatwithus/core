$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "core/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "core"
  s.version     = Core::VERSION
  s.authors     = ["Jaikeerthi"]
  s.email       = ["jai@eatwithus.in"]
  s.homepage    = "http://www.eatwithus.in"
  s.summary     = "Core app"
  s.description = "Core App."
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]

  s.add_dependency "rails"#, "~> 4.1.0"
  s.add_dependency "cloudinary", "1.0.72"
  s.add_dependency "carrierwave-mongoid", "0.7.1"
end
